package com.example.test.ui.main

import android.animation.Animator
import android.graphics.PointF
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewPropertyAnimator
import kotlin.math.abs

class DropAllState(
    override val context: FloatingContext,
    override val floatingViews: ArrayList<FloatingView>
) : FloatingState {
    override val mode: FloatingContainerView.FloatingMode
        get() = FloatingContainerView.FloatingMode.COLLAPSE

    init {
        reset()
    }

    override fun add(floatingView: FloatingView) {
    }

    override fun onTouchEvent(view: View, event: MotionEvent): Boolean {
        return true
    }

    override fun delete(id: Int?) {
    }

    override fun bringToFirst(id: Int?) {
    }

    private fun reset() {
        if (floatingViews.isNotEmpty()) {
            var currentCount = floatingViews.count()
            for (i in 0 until floatingViews.count()) {
                floatingViews[i].layoutParams.width = (mode.size() * context.density).toInt()
                floatingViews[i].layoutParams.height = (mode.size() * context.density).toInt()
                floatingViews[i].requestLayout()
                floatingViews[i].target =  context.droppingPoint
                floatingViews[i].pullingOffset = 0f
                floatingViews[i].pullingView = null
                floatingViews[i].addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
                    val diffX = abs(context.droppingPoint.x - (v.x + v.width / 2))
                    val diffY = abs(context.droppingPoint.y - (v.y + v.height / 2))
                    if (diffX < 5 && diffY < 5) {
                        currentCount--
                        if (currentCount == 0) {
                            vanish()
                        }
                    }
                }
            }
            floatingViews.forEach { it.requestLayout() }
        } else {
            context.state = CollapseStickState(context, ArrayList(), false)
        }
    }

    private fun vanish() {
        var currentCount = floatingViews.count()
        for (i in 0 until floatingViews.count()) {
            floatingViews[i].animate().scaleX(0f).scaleY(0f).setListener(object: Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator?) {
                }

                override fun onAnimationEnd(animation: Animator?) {
                    currentCount--
                    if (currentCount == 0) {
                        context.listener?.floatingsDeleted(floatingViews)
                        context.state = CollapseStickState(context, ArrayList(), false)
                    }
                }

                override fun onAnimationCancel(animation: Animator?) {
                }

                override fun onAnimationRepeat(animation: Animator?) {
                }

            })
        }
    }
}