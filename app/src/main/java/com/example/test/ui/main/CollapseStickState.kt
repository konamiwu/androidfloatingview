package com.example.test.ui.main

import android.content.res.Resources
import android.graphics.PointF
import android.util.DisplayMetrics
import android.view.MotionEvent
import android.view.View


class CollapseStickState(
    override val context: FloatingContext,
    override val floatingViews: ArrayList<FloatingView>,
    private val damping: Boolean
) : FloatingState {
    override val mode: FloatingContainerView.FloatingMode
        get() = FloatingContainerView.FloatingMode.COLLAPSE

    init {
        if (damping)
            reset()
        else
            resetNonDamping()
    }

    override fun add(floatingView: FloatingView) {
        floatingViews.add(floatingView)
        reset()
    }

    override fun onTouchEvent(view: View, event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_MOVE -> {
                context.state = CollapseDraggingState(context, floatingViews)
            }
        }
        return true
    }

    override fun delete(id: Int?) {
        TODO("Not yet implemented")
    }

    override fun bringToFirst(id: Int?) {
        TODO("Not yet implemented")
    }

    private fun reset() {
        val screenWidth = Resources.getSystem().displayMetrics.widthPixels
        val side = if (context.collapsePoint.x > screenWidth / 2) -1 else 1
        if (floatingViews.isNotEmpty()) {
            for (i in 0 until floatingViews.count()) {
                floatingViews[i].targetWidth = (mode.size() * context.density).toInt()
                floatingViews[i].targetHeight = (mode.size() * context.density).toInt()
                floatingViews[i].target =  null
                floatingViews[i].pullingOffset = mode.distance() * context.density * side
                floatingViews[i].pullingView = floatingViews.getOrNull(i + 1)
            }
            floatingViews.first().target = context.collapsePoint
            floatingViews.forEach { it.requestLayout() }
        }
    }

    private fun resetNonDamping() {
        if (floatingViews.isNotEmpty()) {
            for (i in 0 until floatingViews.count()) {
                floatingViews[i].targetWidth = (mode.size() * context.density).toInt()
                floatingViews[i].targetHeight = (mode.size() * context.density).toInt()
                floatingViews[i].target =  PointF(
                    context.collapsePoint.x - mode.distance() * context.density * i,
                    context.collapsePoint.y
                )
                floatingViews[i].pullingOffset = 0f
                floatingViews[i].pullingView = null
                floatingViews[i].requestLayout()
            }
            floatingViews.first().target = context.collapsePoint
        }
    }
}