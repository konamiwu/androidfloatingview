package com.example.test.ui.main

import android.view.MotionEvent
import android.view.View

interface FloatingState {
    val context: FloatingContext
    val floatingViews: ArrayList<FloatingView>
    val mode: FloatingContainerView.FloatingMode
    fun add(floatingView: FloatingView)
    fun onTouchEvent(view: View, event: MotionEvent): Boolean
    fun delete(id: Int?)
    fun bringToFirst(id: Int?)
}