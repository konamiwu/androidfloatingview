package com.example.test.ui.main

import android.graphics.PointF
import android.util.Log
import android.view.MotionEvent
import android.view.VelocityTracker
import android.view.View

class CollapseDraggingState(
    override val context: FloatingContext,
    override val floatingViews: ArrayList<FloatingView>
) : FloatingState {
    private var velocityTracker: VelocityTracker? = null
    private var changeSideVelocityThreshold = 200f

    override val mode: FloatingContainerView.FloatingMode
        get() = FloatingContainerView.FloatingMode.COLLAPSE

    override fun add(floatingView: FloatingView) {
        floatingViews.add(floatingView)
        for (i in 0 until floatingViews.count()) {
            floatingViews[i].target =  null
            floatingViews[i].pullingView = floatingViews.getOrNull(i + 1)
        }
    }

    init {
        if (floatingViews.isNotEmpty()) {
            for (i in 0 until floatingViews.count()) {
                floatingViews[i].targetWidth = (mode.size() * context.density).toInt()
                floatingViews[i].targetHeight = (mode.size() * context.density).toInt()
                floatingViews[i].target =  null
                floatingViews[i].pullingOffset = 0f
                floatingViews[i].pullingView = floatingViews.getOrNull(i + 1)
                floatingViews[i].requestLayout()
            }
        }
    }

    override fun onTouchEvent(view: View, event: MotionEvent): Boolean {
        if (velocityTracker == null) {
            velocityTracker = VelocityTracker.obtain()
        }
        velocityTracker?.addMovement(event)

        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {

            }
            MotionEvent.ACTION_MOVE -> {
                if (floatingViews.isEmpty())
                    return true
                val size = floatingViews.first().width
                floatingViews.first().x = event.x - size / 2
                floatingViews.first().y = event.y - size / 2
                floatingViews.first().requestLayout()
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                if (floatingViews.isEmpty())
                    return true
                val size = floatingViews.first().width.toFloat()

                var m = 1f
                var targetX = 0f
                var targetY = 0f

                velocityTracker?.let {
                    it.computeCurrentVelocity(1000)
                    val xVelocity = it.xVelocity
                    val yVelocity = it.yVelocity
                    m = yVelocity / xVelocity

                    if (xVelocity > changeSideVelocityThreshold) {
                        targetX = view.width.toFloat() - size / 2
                        targetY = m * (view.width - event.x) + event.y - size / 2
                    } else if (xVelocity < -changeSideVelocityThreshold) {
                        targetX = size / 2
                        targetY = m * (0 - event.x) + event.y - size / 2
                    } else {
                        if (event.x > view.width / 2) {
                            targetX = view.width.toFloat() - size / 2
                            targetY = event.y - size / 2
                        } else {
                            targetX = size / 2
                            targetY = event.y - size / 2
                        }
                    }

                    if (targetY > view.height - size) {
                        targetY = view.height - size / 2
                    }
                    if (targetY < size / 2) {
                        targetY = size / 2
                    }
                    context.collapsePoint = PointF(targetX, targetY)
                    velocityTracker?.clear()
                    context.state = CollapseStickState(context, floatingViews, true)
                }

                if (event.y > context.droppingThreshold) {

                }
            }
        }

        return true
    }

    override fun delete(id: Int?) {
    }

    override fun bringToFirst(id: Int?) {
    }

    private fun deleteAll() {
    }
}