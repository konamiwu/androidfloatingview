package com.example.test.ui.main

import android.animation.ValueAnimator
import android.animation.ValueAnimator.AnimatorUpdateListener
import android.content.Context
import android.graphics.PointF
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import kotlin.math.abs
import kotlin.math.pow

class FloatingView: View {
    enum class Side {
        Left,
        Right
    }
    constructor(context: Context, attrs: AttributeSet): super(context, attrs)
    constructor(context: Context): super(context)

    private var edge = Side.Left
    private var changeSideVelocityThreshold = 20f
    private val completeThreshold = 2f
    private val pullAnimationFactor = 0.2f
    private val trackAnimationFactor = 0.08f
    private val sizeAnimationFactor = 0.1f
    private var trackXAnimator: ValueAnimator? = null
    private var trackYAnimator: ValueAnimator? = null
    private var widthFactor = 0.25f
    private var heightFactor = 0.25f
    private var trackXFactor = 0.25f
    private var trackYFactor = 0.25f

    var targetWidth = 0
    var targetHeight = 0
    var pullingView: FloatingView? = null
    var target: PointF? = null
    var pullingOffset = 0f

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        var isNeedLayout = false

        if (right - left != targetWidth) {
            isNeedLayout = true
            val newWidth = linearValue(layoutParams.width.toFloat(), targetWidth.toFloat(), 2)
            layoutParams.width = newWidth.toInt()
        }

        if (bottom - top != targetHeight) {
            isNeedLayout = true
            val newHeight = linearValue(layoutParams.height.toFloat(), targetHeight.toFloat(), 2)
            layoutParams.height = newHeight.toInt()
        }

        target?.let {
            val xDistance = abs(it.x - x - width / 2)
            val yDistance = abs(it.y - y - height / 2)
            if (xDistance < completeThreshold && yDistance < completeThreshold) {
                x = it.x - width / 2
                y = it.y - height / 2
            } else {
                isNeedLayout = false
                val newX = animatedValue(x, it.x - width / 2, trackAnimationFactor)
                val newY = animatedValue(y, it.y - height / 2, trackAnimationFactor)
                x = newX
                y = newY
                post {
                    requestLayout()
                }
            }
        }

        pullingView?.let {
            val xDistance = abs(it.x - x - pullingOffset)
            val yDistance = abs(it.y - y)

            if (xDistance < completeThreshold && yDistance < completeThreshold) {
                it.x = x + pullingOffset
                it.y = y
            } else {
                isNeedLayout = false
                val newX = animatedValue(it.x, x + pullingOffset, pullAnimationFactor)
                val newY = animatedValue(it.y, y, pullAnimationFactor)
                post {
                    it.x = newX
                    it.y = newY
                    pullingView?.requestLayout()
                    requestLayout()
                }
            }
        }

        if (isNeedLayout)
            post { requestLayout() }
    }

    fun setTargetWidth(targetWidth: Int, duration: Long) {
        val diff = abs(width - targetWidth)
        val count = (duration / 1000) / 0.016
        widthFactor = (1 + (1.0 / diff).pow(-count)).toFloat()
        this.targetWidth = targetWidth
    }

    fun setTargetHeight(targetHeight: Int, duration: Long) {
        val diff = abs(height - targetHeight)
        val count = (duration / 1000) / 0.016
        heightFactor = (1 + (1.0 / diff).pow(-count)).toFloat()
        this.targetHeight = targetHeight
    }

    fun setTargetPoint(target: PointF, duration: Long) {
        val diffX = abs(target.x - width / 2 - x)
        val countX = (duration / 1000) / 0.016
        trackXFactor = (1 + (1.0 / diffX).pow(-countX)).toFloat()

        val diffY = abs(target.y - height / 2 - y)
        val countY = (duration / 1000) / 0.016
        trackYFactor = (1 + (1.0 / diffY).pow(-countY)).toFloat()

        this.target = target
    }

    private fun animatedValue(current: Float, target: Float, factor: Float): Float {
        val diff = (target - current)
        var result = diff * factor

        if (result > 0 && result < 1)
            result = 1f
        else if (result < 0 && result > -1)
            result = -1f

        return if (abs(diff) < 1) {
            target
        } else {
            current + result
        }
    }

    private fun linearValue(current: Float, target: Float, size: Int): Float {
        val diff = (target - current)

        return when {
            abs(diff) < 1 -> {
                target
            }
            diff > 0 -> current + size
            else -> current - size
        }
    }
}