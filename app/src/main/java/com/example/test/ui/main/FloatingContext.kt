package com.example.test.ui.main

import android.graphics.PointF

interface FloatingContext {
    fun showDroppingView()
    fun hideDroppingView()
    fun animateDroppingView()
    fun delete(id: Int?)
    val droppingPoint: PointF
    val droppingThreshold: Float
    var state: FloatingState
    var collapsePoint: PointF
    val expandPoint: PointF
    val maxCount: Int
    val density: Float
    var listener: FloatingListener?

    interface FloatingListener {
        fun floatingDidDelete(id: Int?)
        fun floatingsDeleted(floatingViews: ArrayList<FloatingView>)
        fun floatingsDidExpand()
        fun floatingsDidCollapse()
        fun floatingsNeedCollapse()
        fun floatingDidSelect(id: Int?)
    }
}