package com.example.test.ui.main

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PointF
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.widget.FrameLayout
import com.example.test.R


@SuppressLint("ClickableViewAccessibility")
class FloatingContainerView: FrameLayout, FloatingContext {
    constructor(context: Context, attrs: AttributeSet): super(context, attrs)
    constructor(context: Context): super(context)

    enum class FloatingMode {
        EXPAND {
            override fun distance(): Float {
                return 44f
            }

            override fun size(): Float {
                return 36f
            }
        },
        COLLAPSE {
            override fun distance(): Float {
                return 3f
            }

            override fun size(): Float {
                return 50f
            }
        };

        abstract fun distance(): Float
        abstract fun size(): Float
    }

    private val paddingRight = 10f
    private val paddingTop = 10f

    override val droppingPoint: PointF
        get() = PointF(width / 2.toFloat(), height.toFloat() / 4 * 3)
    override val droppingThreshold: Float
        get() = 0f
    override var state: FloatingState = CollapseStickState(this, ArrayList<FloatingView>(), false)
    override var collapsePoint: PointF = PointF(0f, 0f)
    override val expandPoint: PointF
        get()  {
            val size = FloatingMode.EXPAND.size() * density
            return PointF(
                width - size / 2 - paddingRight * density,
                paddingTop * density + size / 2
            )
        }
    override val maxCount: Int
        get() = 0
    override var listener: FloatingContext.FloatingListener? = null

    init {
        setOnTouchListener { view, event ->
            state.onTouchEvent(view, event)
        }
        listener = object: FloatingContext.FloatingListener {
            override fun floatingDidDelete(id: Int?){}
            override fun floatingsDeleted(floatingViews: ArrayList<FloatingView>){
                removeAllViews()
            }
            override fun floatingsDidExpand(){}
            override fun floatingsDidCollapse(){}
            override fun floatingsNeedCollapse(){}
            override fun floatingDidSelect(id: Int?){}
        }
    }

    fun add() {
        val floating = FloatingView(context)
        val size = state.mode.size() * density
        floating.layoutParams = LayoutParams(size.toInt(), size.toInt())
        floating.targetWidth = size.toInt()
        floating.targetHeight = size.toInt()
        when {
            state.floatingViews.count() % 3 == 0 -> floating.setBackgroundResource(R.drawable.blue_circle)
            state.floatingViews.count() % 3 == 1 -> floating.setBackgroundResource(R.drawable.green_circle)
            else -> floating.setBackgroundResource(R.drawable.red_circle)
        }
        addView(floating, 0)

        state.add(floating)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if (collapsePoint.x == 0f && collapsePoint.y == 0f) {
            val size = FloatingMode.COLLAPSE.size() * density
            collapsePoint = PointF(width - size.toFloat() / 2, height.toFloat() / 2)
        }
    }

    override fun showDroppingView() {
        TODO("Not yet implemented")
    }

    override fun hideDroppingView() {
        TODO("Not yet implemented")
    }

    override fun animateDroppingView() {
        TODO("Not yet implemented")
    }

    override fun delete(id: Int?) {
        TODO("Not yet implemented")
    }

    override val density: Float
        get() = context.resources.displayMetrics.density

    fun expand() {
        state = ExpandStickState(this, state.floatingViews)
    }

    fun collapse() {
        val size = FloatingMode.COLLAPSE.size() * density
//        collapsePoint = PointF(width - size.toFloat() / 2, height.toFloat() / 2)
        state = CollapseStickState(this, state.floatingViews, false)
    }
    fun dropAll() {
        state = DropAllState(this, state.floatingViews)
    }
}