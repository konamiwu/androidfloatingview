package com.example.test.ui.main

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import com.example.test.R

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var floatingContainer: FloatingContainerView
    private lateinit var addButton: Button
    private lateinit var expandButton: Button
    private lateinit var collapseButton: Button
    private lateinit var dropAllButton: Button

    private val size = 120f
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val main = inflater.inflate(R.layout.main_fragment, container, false)
        floatingContainer = main.findViewById(R.id.floatingContainer)
        addButton = main.findViewById(R.id.addButton)
        expandButton = main.findViewById(R.id.expandButton)
        collapseButton = main.findViewById(R.id.collapseButton)
        dropAllButton = main.findViewById(R.id.dropAllButton)

        addButton.setOnClickListener {
            floatingContainer.add()
        }
        expandButton.setOnClickListener {
            floatingContainer.expand()
        }
        collapseButton.setOnClickListener {
            floatingContainer.collapse()
        }
        dropAllButton.setOnClickListener {
            floatingContainer.dropAll()
        }
        return main
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel
    }
}