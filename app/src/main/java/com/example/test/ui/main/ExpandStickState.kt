package com.example.test.ui.main

import android.graphics.PointF
import android.view.MotionEvent
import android.view.View

class ExpandStickState(
    override val context: FloatingContext,
    override val floatingViews: ArrayList<FloatingView>
) : FloatingState {
    override val mode: FloatingContainerView.FloatingMode
        get() = FloatingContainerView.FloatingMode.EXPAND

    init {
        reset()
    }

    override fun add(floatingView: FloatingView) {
        floatingViews.add(floatingView)
        reset()
    }

    override fun onTouchEvent(view: View, event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_MOVE -> {
                context.state = CollapseDraggingState(context, floatingViews)
            }
        }
        return true
    }

    override fun delete(id: Int?) {
    }

    override fun bringToFirst(id: Int?) {

    }

    private fun reset() {
        if (floatingViews.isNotEmpty()) {
            for (i in 0 until floatingViews.count()) {
                floatingViews[i].targetWidth = (mode.size() * context.density).toInt()
                floatingViews[i].targetHeight = (mode.size() * context.density).toInt()
                floatingViews[i].target =  getExpandPoint(i)
                floatingViews[i].pullingOffset = 0f
                floatingViews[i].pullingView = null
                floatingViews[i].requestLayout()
            }
        }
    }

    private fun getExpandPoint(index: Int) : PointF {
        val interval = mode.distance() * context.density
        return if (index == 0) {
            context.expandPoint
        } else {
            PointF(context.expandPoint.x - (interval) * index, context.expandPoint.y)
        }
    }
}